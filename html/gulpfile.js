var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minifycss = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename');

gulp.task('sass', function () {
    return gulp.src('./sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});
gulp.task('mini', function () {
    return gulp.src('css/*.css')
        .pipe(concat('app.css'))
        .pipe(gulp.dest('mini/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('mini/'));
});

gulp.task('watch', function () {
    gulp.watch('./sass/**/*.scss', ['sass']);
    gulp.watch('./sass/**/*.scss', ['mini']);
});
gulp.task('default', ['sass'], function () {
    gulp.run('sass', 'watch','mini');
});
